/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network.ingame;

import game.Game;
import game.network.serialization.types.*;

public class GetSendDataAsClient
{
	private static boolean disconnect = false;

	public static byte[] getData(SClass sClass)
	{
		if (disconnect) disconnect(sClass);
		else if (Game.getLevel() == null) requestJoin(sClass);
		else
		{
			tickPlayer(sClass);
			AbilityOnline.tick(sClass);
		}

		byte[] data = new byte[sClass.getSize()];
		sClass.getBytes(data, 0);

		return data;
	}

	private static void disconnect(SClass sClass)
	{
		SObject disconnectObj = new SObject(SObjectType.DISCONNECT);

		sClass.addObject(disconnectObj);
		disconnect = false;
	}

	public static void disconnect()
	{
		disconnect = true;
	}

	private static void requestJoin(SClass sClass)
	{
		SObject requestJoin = new SObject(SObjectType.REQUEST_JOIN);
		requestJoin.addString(SString.String("name", "player123")); //TODO: Custom name
		sClass.addObject(requestJoin);
	}

	private static void tickPlayer(SClass sClass)
	{
		SObject player = new SObject(SObjectType.GUEST_PLAYER);

		player.addField(SField.Float("xVel", Game.getLevel().getClientPlayer().getXVelocity()));
		player.addField(SField.Float("yVel", Game.getLevel().getClientPlayer().getYVelocity()));

		sClass.addObject(player);
	}
}
