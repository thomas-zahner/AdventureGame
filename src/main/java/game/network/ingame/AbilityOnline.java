/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network.ingame;

import game.Game;
import game.entity.mob.ability.Ability;
import game.entity.mob.ability.AbilityShooting;
import game.entity.mob.player.Player;
import game.entity.projectile.ProjectileBoomerang;
import game.network.serialization.types.SClass;
import game.network.serialization.types.SField;
import game.network.serialization.types.SObject;
import game.network.serialization.SerializationReader;
import game.network.serialization.types.SObjectType;

public class AbilityOnline
{
	private static Ability[] abilities = new Ability[3];

	public static void tick(SClass sClass)
	{
		serialize(sClass);
		reset();
	}

	public static void trigger(Ability ability, int abilityNumber)
	{
		abilities[abilityNumber] = ability;
	}

	private static void serialize(SClass sClass)
	{
		for (int i = 0; i < abilities.length; i++)
		{
			if (abilities[i] != null)
			{
				SObject object = new SObject(SObjectType.ABILITY);
				object.addField(SField.Byte("number", (byte) i));

				if (abilities[i] instanceof AbilityShooting)
				{
					object = ((AbilityShooting) abilities[i]).serialize(object);
				}

				sClass.addObject(object);
			}
		}
	}

	private static void reset()
	{
		for (int i = 0; i < abilities.length; i++) abilities[i] = null;
	}

	public static void deserialize(SClass sClass, String IPAddressSender)
	{
		for (SObject object : sClass.getObjects(SObjectType.ABILITY))
		{
			SField numberField = object.findField("number");
			if (numberField == null) continue;

			byte number = SerializationReader.readByte(numberField.getData(), 0);
			Ability ability = translate(IPAddressSender, number);

			if (ability != null)
			{
				if (ability instanceof AbilityShooting) ((AbilityShooting) ability).deserialize(object);
				ability.enable(number);
			}
		}
	}

	private static Ability translate(String IPAddressSender, byte number)
	{
		Player player = Game.getLevel().getPlayerByIP(IPAddressSender);
		return player.getAbility(number);
	}
}
