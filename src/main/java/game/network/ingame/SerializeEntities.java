/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network.ingame;

import game.Game;
import game.entity.Entity;
import game.entity.damage.Damage;
import game.entity.mob.Mob;
import game.entity.mob.player.OnlinePlayer;
import game.entity.mob.player.Player;
import game.entity.projectile.Projectile;
import game.entity.spawner.ParticleSpawner;
import game.network.serialization.Serializable;
import game.network.serialization.types.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SerializeEntities
{
	public static void tick(SClass sClass)
	{
		List<Mob> mobs = Collections.synchronizedList(Game.getLevel().getEntitiesOfType(Mob.class));
		List<Projectile> projectiles = Collections.synchronizedList(Game.getLevel().getEntitiesOfType(Projectile.class));
		List<Damage> damages = Collections.synchronizedList(Game.getLevel().getEntitiesOfType(Damage.class));

		for (Mob mob : mobs) serialize(mob, sClass);
		for (Projectile projectile : projectiles) serialize(projectile, sClass);
		for (Damage damage : damages) serialize(damage, sClass);

		entityDeletion(Game.getLevel().getRemovedEntities(), sClass);
	}

	private static void serialize(Mob mob, SClass sClass)
	{
		SObject object = mob.serialize();

		SString type = object.findString("type");

		if (type.getValue().equals(OnlinePlayer.class.getSimpleName()))
		{
			type.setValue(Player.class.getSimpleName());
		}

		if (mob.equals(Game.getLevel().getClientPlayer()))
		{
			object.addString(SString.String("name", "host"));
		}

		sClass.addObject(object);
	}

	private static <T extends Serializable> void serialize(T serializable, SClass sClass)
	{
		SObject object = serializable.serialize();
		sClass.addObject(object);
	}

	private static void entityDeletion(List<Entity> removedEntities, SClass sClass)
	{
		ArrayList<Entity> entities = new ArrayList<>(removedEntities);

		entities.forEach(entity ->
		{
			if (!(entity instanceof Projectile || entity instanceof Mob)) return;

			String uuid = entity.getUUID().toString();
			SObject object = new SObject(SObjectType.ENTITY_DELETION);
			object.addString(SString.String("uuid", uuid));
			sClass.addObject(object);
		});
	}
}
