/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network.ingame;

import game.Game;
import game.entity.Entity;
import game.entity.damage.Damage;
import game.entity.mob.Guardian;
import game.entity.mob.Mob;
import game.entity.mob.Slime;
import game.entity.mob.player.OnlinePlayer;
import game.entity.mob.player.Player;
import game.entity.projectile.*;
import game.entity.spawner.ParticleSpawner;
import game.level.Level;
import game.network.serialization.types.SClass;
import game.network.serialization.types.SObject;
import game.network.serialization.types.SObjectType;
import game.network.serialization.types.SString;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DeserializeEntities
{
	private static Level level = Game.getLevel();

	public static void tick(SClass sClass)
	{
		for (SObject object : sClass.getObjects(SObjectType.MOB)) deserializeMobs(object);
		for (SObject object : sClass.getObjects(SObjectType.PROJECTILE)) deserializeProjectiles(object);
		for (SObject object : sClass.getObjects(SObjectType.DAMAGE)) deserializeDamage(object);

		for (SObject object : sClass.getObjects(SObjectType.ENTITY_DELETION)) entityDeletion(object);
	}

	private static void entityDeletion(SObject object)
	{
		SString uuid = object.findString("uuid");

		if (uuid != null)
		{
			List<Entity> arrayList = new ArrayList<>(level.getEntities());
			arrayList.stream().filter(entity -> entity.getUUID().toString().equals(uuid.getValue())).findFirst().ifPresent(target -> level.remove(target));
		}
	}

	private static void deserializeMobs(SObject object)
	{
		SString uuid = object.findString("uuid");
		if (uuid == null) return;

		Mob mob = level.getMob(UUID.fromString(uuid.getValue()));

		if (mob == null)
		{
			mob = translateMob(object, UUID.fromString(uuid.getValue()));
			if (mob == null) return;

			level.add(mob);
		}
		else if (mob.equals(level.getClientPlayer()))
		{
			object.removeField("xVel");
			object.removeField("yVel");
		}

		mob.deserialize(object);
	}

	private static void deserializeProjectiles(SObject object)
	{
		SString uuid = object.findString("uuid");
		if (uuid == null) return;

		Projectile projectile = level.getProjectile(UUID.fromString(uuid.getValue()));

		if (projectile == null)
		{
			projectile = translateProjectile(object, UUID.fromString(uuid.getValue()));
			if (projectile == null) return;

			level.add(projectile);
		}

		projectile.deserialize(object);
	}

	private static void deserializeDamage(SObject object)
	{
		Damage damage = new Damage(null, 0);
		damage.deserialize(object);
		level.add(damage);
	}


	private static Mob translateMob(SObject object, UUID uuid)
	{
		SString sType = object.findString("type");
		if (sType == null) return null;

		String type = sType.getValue();

		if (type.equals(Player.class.getSimpleName())) return new OnlinePlayer(0, 0, uuid, "");
		else if (type.equals(Slime.class.getSimpleName())) return new Slime(0, 0, uuid);
		else if (type.equals(Guardian.class.getSimpleName())) return new Guardian(0, 0, uuid);

		return null;
	}

	private static Projectile translateProjectile(SObject object, UUID uuid)
	{
		SString sType = object.findString("type");
		if (sType == null) return null;

		String type = sType.getValue();

		if (type.equals(ProjectileBullet.class.getSimpleName())) return new ProjectileBullet(0, 0, 0, null, uuid);
		else if (type.equals(ProjectileGuardian.class.getSimpleName()))
			return new ProjectileGuardian(0, 0, 0, null, uuid);
		else if (type.equals(ProjectileBoomerang.class.getSimpleName()))
			return new ProjectileBoomerang(0, 0, 0, null, uuid);
		else if (type.equals(ProjectileGrenade.class.getSimpleName()))
			return new ProjectileGrenade(0, 0, 0, null, uuid);

		return null;
	}
}

