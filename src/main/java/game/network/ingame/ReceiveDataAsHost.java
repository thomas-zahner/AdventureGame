/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.network.ingame;

import game.Game;
import game.entity.mob.player.OnlinePlayer;
import game.entity.mob.player.Player;
import game.level.Level;
import game.network.NetworkPackage;
import game.network.Server;
import game.network.serialization.types.*;
import game.network.serialization.SerializationReader;

public class ReceiveDataAsHost
{
	public static void receiveData(byte[] data, String IPAddressSender)
	{
		Level level = Game.getLevel();

		if (level == null) return;
		if (Server.isClientBanned(IPAddressSender)) return;

		SClass sClass = SClass.deserialize(data, 0);
		if (sClass == null || !sClass.getName().equals(NetworkPackage.CLASS_NAME)) return;

		if (!Server.isClientOnline(IPAddressSender))
		{
			handlePlayerJoin(sClass, IPAddressSender, level);
			return;
		}

		if (sClass.getObject(SObjectType.DISCONNECT) != null)
		{
			disconnectPlayer(IPAddressSender, level);
		}

		tickPlayer(sClass, IPAddressSender, level);
		AbilityOnline.deserialize(sClass, IPAddressSender);
		Server.updateLastSuccessfulCommunication(IPAddressSender);
	}

	private static void handlePlayerJoin(SClass sClass, String IPAddressSender, Level level)
	{
		SObject requestJoin = sClass.getObject(SObjectType.REQUEST_JOIN);
		if (requestJoin == null) return;

		SString name = requestJoin.findString("name");
		if (name == null) return;

		Server.addClient(IPAddressSender);

		level.add(new OnlinePlayer(level.getSpawnLocation().getX(), level.getSpawnLocation().getY(), IPAddressSender, name.getValue()));
		Game.getPrinter().printImportantInfo(name.getValue() + " joined the game!");
	}

	private static void disconnectPlayer(String IPAddressSender, Level level)
	{
		Server.removeClient(IPAddressSender);
		level.getPlayerByIP(IPAddressSender).remove();
		Game.getPrinter().printImportantInfo(((OnlinePlayer) level.getPlayerByIP(IPAddressSender)).getPlayerName() + " disconnected from the game!");
	}

	private static void tickPlayer(SClass sClass, String IPAddressSender, Level level)
	{
		Player senderPlayer = level.getPlayerByIP(IPAddressSender);

		SObject player = sClass.getObject(SObjectType.GUEST_PLAYER);
		if (player == null) return;

		SField xVel = player.findField("xVel");
		SField yVel = player.findField("yVel");

		if (xVel != null && yVel != null)
		{
			float xVelocity = SerializationReader.readFloat(xVel.getData(), 0);
			float yVelocity = SerializationReader.readFloat(yVel.getData(), 0);

			//TODO: Anti cheat and stuff

			if (Math.abs(xVelocity) <= senderPlayer.getSpeed() && Math.abs(yVelocity) <= senderPlayer.getSpeed())
				senderPlayer.motion(xVelocity, yVelocity);
			else Game.getPrinter().printInfo("Anti cheat detected illegal movement: " + xVelocity + " " + yVelocity);
		}
	}
}
