/*
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package game.entity.item;

import game.graphics.Screen;
import game.graphics.Sprite;

public class ItemStack
{
	private int itemCount = 0;
	private static final int MAX_ITEM_COUNT = 9;

	private Item itemInstance = null;
	private ItemType requiredItemType = ItemType.None;

	public ItemStack()
	{
	}

	public ItemStack(ItemType requiredItemType)
	{
		this.requiredItemType = requiredItemType;
	}

	/**
	 * Attempt to add an item to the stack.
	 */
	public boolean add(Item item)
	{
		if (!isValidItemType(item)) return false;

		if (!isEmpty() && this.itemInstance.getClass() != item.getClass()) return false;
		if (this.isFull()) return false;

		if (isEmpty()) itemInstance = item;
		itemCount++;
		return true;
	}

	/**
	 * Return whether successful or not
	 */
	public boolean merge(ItemStack other)
	{
		//Only allow same items to merge
		if (!isEmpty() && this.itemInstance.getClass() != other.itemInstance.getClass()) return false;

		if (!isValidItemType(other.itemInstance)) return false;
		if (this.isFull() || other.isFull()) return false;

		while (!other.isEmpty())
		{
			if (this.isFull()) break;

			if (isEmpty()) this.itemInstance = other.itemInstance;
			other.itemCount--;
			this.itemCount++;
		}

		if (other.isEmpty()) other.clear();
		return true;
	}

	public boolean swap(ItemStack other)
	{
		if (!isValidItemType(other.itemInstance)) return false;
		if (this.itemInstance == null && other.itemInstance == null) return false;

		int tempCount = other.itemCount;
		Item tempItemInstance = other.itemInstance;

		other.itemCount = this.itemCount;
		other.itemInstance = this.itemInstance;

		this.itemCount = tempCount;
		this.itemInstance = tempItemInstance;
		return true;
	}

	public void render(Screen screen, int x, int y)
	{
		if (itemInstance == null) return;

		screen.renderSprite(x, y, itemInstance.getSprite(), false);
		Sprite.writeValues(String.valueOf(itemCount), screen, x + 9, y + 9, 0x000000);
	}

	public Item getItemInstance()
	{
		return itemInstance;
	}

	public int getItemCount()
	{
		return itemCount;
	}

	public boolean isEmpty()
	{
		return itemCount <= 0;
	}

	public boolean isFull()
	{
		return itemCount >= MAX_ITEM_COUNT;
	}

	private boolean isValidItemType(Item item)
	{
		if (this.requiredItemType == ItemType.None) return true;
		if (item == null) return true;
		return this.requiredItemType == item.getType();
	}

	private void clear()
	{
		itemCount = 0;
		itemInstance = null;
	}
}
