/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game;

import game.audio.PlaySound;
import game.chat.Chat;
import game.graphics.gui.GUI;
import game.graphics.HUD;
import game.graphics.Screen;
import game.graphics.screens.ScreenInfo;
import game.graphics.screens.ScreenTitle;
import game.graphics.screens.selection.SelectionInfo;
import game.input.Keyboard;
import game.input.Mouse;
import game.level.Level;
import game.level.LevelLoader;
import game.network.Connection;
import game.network.Server;
import game.settings.Settings;
import game.util.GameState;
import game.util.Print;
import game.util.PropertyLoader;
import game.util.Resource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Random;

public class Game extends Canvas implements Runnable
{
	public static int width, height;
	public static final byte SCALE = 4;
	private static String VERSION;

	private static boolean running = false;
	private static int currentFPS = 0, currentTPS = 0;
	private static float currentFrameTime = 0;

	private JFrame frame;
	private static Thread thread;

	private static Print printer = new Print();
	private static Resource resource = new Resource();

	private static Screen screen;
	private static HUD hud;
	private static GUI activeGui;

	private static Keyboard key;
	private static Mouse mouse = new Mouse();

	private static Level level;

	private static Connection connection = null;
	private static boolean multiplayer = false;
	public static boolean isHostingGame;

	private BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); //Creating image
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData(); //Accessing image

	public static final byte TPS = 60;
	private static int gameStateTicksPassed = -1;
	private static GameState gameState = GameState.TitleScreen;

	private Game()
	{
		Dimension size = new Dimension(width * SCALE, height * SCALE);
		setPreferredSize(size);

		VERSION = PropertyLoader.fromFile("/project.properties").getValue("version");
		if (VERSION == null) VERSION = "UNKNOWN";

		frame = new JFrame();
		key = new Keyboard();

		screen = new Screen(width, height);

		addKeyListener(key);

		addMouseListener(mouse);
		addMouseMotionListener(mouse);

		PlaySound.initAudioSystem();
	}

	public synchronized void start()
	{
		running = true;
		thread = new Thread(this, "Game");
		thread.start();
		printer.printInfo("Launched game");
	}

	public static synchronized void stop()
	{
		printer.printInfo("Stopping game");
		if (connection != null) connection.close();
		running = false;
	}

	public void run()
	{
		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		final double nsPerTick = 1_000_000_000.0 / TPS;
		double delta = 0;
		int fpsCount = 0;
		int tpsCount = 0;

		while (running)
		{
			long now = System.nanoTime();
			long frameTimeStart;
			long frameTimeEnd;
			delta += (now - lastTime) / nsPerTick;
			lastTime = now;

			//Waits until (1 / ticks per second) of a second is passed
			while (delta >= 1)
			{
				tick();
				tpsCount++;
				delta--;
			}

			frameTimeStart = System.nanoTime();

			render();
			fpsCount++;

			frameTimeEnd = System.nanoTime();

			if (System.currentTimeMillis() - timer >= 1000)
			{
				timer += 1000;
				currentFPS = fpsCount;
				currentTPS = tpsCount;
				tpsCount = 0;
				fpsCount = 0;

				currentFrameTime = Math.round((frameTimeEnd - frameTimeStart) / 1_000_000.0F * 100F) / 100F;
			}
		}

		System.exit(0);
	}

	public void tick()
	{
		gameStateTicksPassed++;

		if (gameState == GameState.TitleScreen) ScreenTitle.tick(key);
		else if (gameState == GameState.ScreenInfo) ScreenInfo.tick(key);
		else if (gameState == GameState.IngameOnline || gameState == GameState.IngameOffline)
		{
			if (gameState == GameState.IngameOffline && level == null) loadLevel(0);
			if (gameState == GameState.IngameOnline && !multiplayer) return;

			Mouse.tick();
			key.tick();
			level.tick();
			hud.tick();
			screen.tick();

			if (isHostingGame) Server.tick();
			if (activeGui != null) activeGui.tick();

			if (key.escapeToggle && !level.getClientPlayer().isTypingMessage())
			{
				if (gameState == GameState.IngameOnline) abortMultiplayer(null);
				else unloadLevel();
				if (gameState != GameState.ScreenInfo) setGameState(GameState.TitleScreen);
			}
		}
	}

	public void render()
	{
		BufferStrategy bs = getBufferStrategy();
		if (bs == null)
		{
			createBufferStrategy(Settings.bufferStrategy.getValue()); //Creates the first time the render method runs a double buffer, triple buffer, etc.
			return;
		}
		Graphics g = bs.getDrawGraphics(); //Creates a link between the buffer and graphics

		screen.clear();

		if (gameState == GameState.TitleScreen) ScreenTitle.render(screen);
		else if (gameState == GameState.ScreenInfo) ScreenInfo.render(screen);
		else if (gameState == GameState.IngameOffline || gameState == GameState.IngameOnline)
		{
			if (level == null || hud == null) return;
			level.render(screen);
			hud.render(screen);
		}

		//Filters are to place here!
		screen.applyBrightness();

		//Ingame GUI shouldn't be affected by the brightness
		if (gameState == GameState.IngameOffline || gameState == GameState.IngameOnline) screen.renderGUI(activeGui);

		System.arraycopy(screen.pixels, 0, pixels, 0, pixels.length);
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null); //Draw rendered stuff to the buffer

		//Draw with graphics g on the same buffer
		if ((gameState == GameState.IngameOffline || gameState == GameState.IngameOnline) && level != null)
		{
			hud.render(g, Settings.debugMode.getValue());
		}

		g.dispose(); //Removes the graphics (not the buffer)
		bs.show(); //Shows and swaps the buffers
	}

	public static void launchWindowedGame(int width, int height)
	{
		Game.width = width;
		Game.height = height;

		Game game = new Game();
		makeWindow(game, false);
		game.start();
	}

	public static void launchFullscreenGame()
	{
		Game.width = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getBounds().width / SCALE;
		Game.height = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getBounds().height / SCALE;

		Game game = new Game();
		GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(Window.getWindows()[0]);

		makeWindow(game, true);
		game.start();
	}

	private static void makeWindow(Game game, boolean fullscreen)
	{
		Image cursor = Toolkit.getDefaultToolkit().getImage(getResource().getAbsolutePath("/textures/cursors/cursor.png")).getScaledInstance(32, 32, 0);
		game.frame.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(cursor, new Point(game.frame.getX(), game.frame.getY()), "cursor"));

		game.frame.setTitle("2D Adventure" + " (" + VERSION + ")");

		game.frame.add(game);
		game.frame.pack(); //Apply the size to the window

		if (!fullscreen)
		{
			game.frame.setResizable(false);
			game.frame.setLocationRelativeTo(null); //Centers the frame
		}

		game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Makes that the process gets terminated when closing the window
		game.frame.setVisible(true);

		game.frame.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent windowEvent)
			{
				stop();

				try
				{
					thread.join();
				}
				catch (InterruptedException e)
				{
					printer.printError(e.getMessage());
				}
			}
		});

		game.requestFocus(); //So you don't have to click inside the window in order to get key input working
	}

	public static void startServer()
	{
		multiplayer = true;
		isHostingGame = true;
		startMultiplayer(null);

		if (connection != null)
		{
			loadLevel(0);
			if (gameState != GameState.ScreenInfo) setGameState(GameState.IngameOnline);
		}
	}

	public static void connectToServer(String hostIp)
	{
		unloadLevel();
		multiplayer = true;
		isHostingGame = false;
		startMultiplayer(hostIp);
	}

	public static void loadLevel(long seed)
	{
		Random rand = new Random();
		level = new LevelLoader().fromSeed(seed == 0 ? rand.nextLong() : seed).load(key, "GeneratedLevel");

		hud = new HUD(width, height, level, key);
		Chat.init();
	}

	public static void loadTitleScreenLevel()
	{
		Random rand = new Random();
		level = new LevelLoader().fromSeed(rand.nextLong()).load(key, "TitleScreen");
	}

	public static void unloadLevel()
	{
		level = null;
		connection = null;
		multiplayer = false;
	}

	/**
	 * Start multiplayer. Automatically abort if any problem occurs.
	 *
	 * @param ip The host ip as string or null if hosting a game.
	 */
	private static void startMultiplayer(String ip)
	{
		connection = new Connection(ip, isHostingGame);
		connection.connect();

		if (multiplayer)
		{
			setGameState(GameState.IngameOnline);
			connection.startThread();
		}
	}

	public static void abortMultiplayer(String reason)
	{
		if (connection != null) connection.close();
		unloadLevel();

		if (reason != null)
		{
			ScreenTitle.setSelectionScreen(new SelectionInfo(reason, ScreenTitle.getCurrentSelectionScreen()));
			printer.printError(reason);
		}
	}

	public static Level getLevel()
	{
		return Game.level;
	}

	public static Screen getScreen()
	{
		return Game.screen;
	}

	public static Connection getConnection()
	{
		return connection;
	}

	public static Resource getResource()
	{
		return resource;
	}

	public static int getCurrentFPS()
	{
		return currentFPS;
	}

	public static int getCurrentTPS()
	{
		return currentTPS;
	}

	public static float getCurrentFrameTime()
	{
		return currentFrameTime;
	}

	public static String getVersion()
	{
		return VERSION;
	}

	public static void setGameState(GameState gameState)
	{
		gameStateTicksPassed = -1;
		Game.gameState = gameState;
	}

	public static void setActiveGUI(GUI gui)
	{
		Game.activeGui = gui;
	}

	public static GameState getGameState()
	{
		return Game.gameState;
	}

	public static GUI getActiveGUI()
	{
		return Game.activeGui;
	}

	public static int getGameStateTicksPassed()
	{
		return gameStateTicksPassed;
	}

	public static Print getPrinter()
	{
		return printer;
	}
}
