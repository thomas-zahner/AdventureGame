package game.graphics.screens.selection;

import game.Game;
import game.input.Keyboard;
import game.input.TextInput;

public class SelectionServer extends SelectionScreen
{
	public SelectionServer(SelectionScreen parent)
	{
		super(new String[]{""}, parent);
		TextInput.startRecordingInput();
	}

	@Override
	public void tick(Keyboard input)
	{
		super.tick(input);
		setSelections(new String[]{TextInput.getTextInput()});
	}

	@Override
	protected void onExit()
	{
		super.onExit();
		TextInput.stopRecordingInput();
	}

	@Override
	protected void onResume()
	{
		TextInput.startRecordingInput();
	}

	@Override
	public void onTriggerSelection()
	{
		Game.connectToServer(TextInput.getTextInput());
		TextInput.stopRecordingInput();
	}

	@Override
	public void onIncreaseCurrentSelection()
	{
	}

	@Override
	public void onDecreaseCurrentSelection()
	{
	}
}
