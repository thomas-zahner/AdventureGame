package game.graphics.screens.selection;

import game.Game;
import game.level.persistence.LevelReader;
import game.util.GameState;

import java.util.ArrayList;
import java.util.List;

public class SelectionLevel extends SelectionScreen
{
	private final static String NEW_LEVEL = "Create new level";

	public SelectionLevel(SelectionScreen parent, LevelReader levelReader)
	{
		super(parent);

		if (levelReader.getNumberOfLevels() == 0) setSelections(new String[]{NEW_LEVEL});
		else
		{
			List<String> selections = new ArrayList<>(levelReader.getLevelNames());
			selections.add(NEW_LEVEL);

			setSelections(selections.toArray(new String[0]));
		}
	}

	@Override
	public void onTriggerSelection()
	{
		if (getSelections()[getCurrentSelection()].equals(NEW_LEVEL))
		{
			Game.unloadLevel();
			Game.setGameState(GameState.IngameOffline);
		}
	}

	@Override
	public void onIncreaseCurrentSelection()
	{
	}

	@Override
	public void onDecreaseCurrentSelection()
	{
	}
}
