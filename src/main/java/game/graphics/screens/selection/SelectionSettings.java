package game.graphics.screens.selection;

import game.settings.*;

public class SelectionSettings extends SelectionScreen
{
	private Setting[] settings = Settings.getSettings();

	public SelectionSettings(SelectionScreen parent)
	{
		super(parent);
		setSelections(getSettingsAsString());
	}

	@Override
	public void onTriggerSelection()
	{
		onIncreaseCurrentSelection();
	}

	@Override
	public void onIncreaseCurrentSelection()
	{
		Setting setting = settings[getCurrentSelection()];

		if (setting instanceof SettingBoolean) ((SettingBoolean) setting).toggleValue();
		else if (setting instanceof SettingRange) ((SettingRange) setting).increaseValue();

		update();
	}

	@Override
	public void onDecreaseCurrentSelection()
	{
		Setting setting = settings[getCurrentSelection()];

		if (setting instanceof SettingBoolean) ((SettingBoolean) setting).toggleValue();
		else if (setting instanceof SettingRange) ((SettingRange) setting).decreaseValue();

		update();
	}

	private void update()
	{
		setCurrentSelection(settings[getCurrentSelection()].toString());
		SettingsWriter.writeSettings();
	}

	private String[] getSettingsAsString()
	{
		String[] strings = new String[settings.length];

		for (int i = 0; i < settings.length; i++)
		{
			strings[i] = settings[i].toString();
		}

		return strings;
	}
}
