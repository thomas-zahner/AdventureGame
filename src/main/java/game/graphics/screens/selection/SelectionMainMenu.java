package game.graphics.screens.selection;

import game.Game;
import game.graphics.screens.ScreenTitle;
import game.level.persistence.LevelReader;

public class SelectionMainMenu extends SelectionScreen
{
	private static final String SINGLEPLAYER = "Singleplayer";
	private static final String MULTIPLAYER = "Multiplayer";
	private static final String SETTINGS = "Settings";
	private static final String QUIT = "Quit";

	public SelectionMainMenu(SelectionScreen parent)
	{
		super(new String[]{SINGLEPLAYER, MULTIPLAYER, SETTINGS, QUIT}, parent);
	}

	@Override
	public void onTriggerSelection()
	{
		switch (getSelections()[getCurrentSelection()])
		{
			case SINGLEPLAYER:
				LevelReader levelReader = new LevelReader();
				ScreenTitle.setSelectionScreen(new SelectionLevel(this, levelReader));
				break;

			case MULTIPLAYER:
				ScreenTitle.setSelectionScreen(new SelectionOnline(this));
				break;

			case SETTINGS:
				ScreenTitle.setSelectionScreen(new SelectionSettings(this));
				break;

			case QUIT:
				Game.stop();
				break;
		}
	}

	@Override
	public void onIncreaseCurrentSelection()
	{
	}

	@Override
	public void onDecreaseCurrentSelection()
	{
	}
}
