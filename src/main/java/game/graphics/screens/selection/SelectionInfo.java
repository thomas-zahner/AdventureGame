package game.graphics.screens.selection;

public class SelectionInfo extends SelectionScreen
{

	public SelectionInfo(String info, SelectionScreen parent)
	{
		super(new String[]{info}, parent);
	}

	@Override
	public void onTriggerSelection()
	{
		onExit();
	}

	@Override
	public void onIncreaseCurrentSelection()
	{
	}

	@Override
	public void onDecreaseCurrentSelection()
	{
	}
}
