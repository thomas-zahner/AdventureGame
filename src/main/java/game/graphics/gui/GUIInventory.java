/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.graphics.gui;

import game.Game;
import game.entity.item.Item;
import game.entity.item.ItemAbilityProjectileBullet;
import game.entity.item.ItemStack;
import game.entity.item.ItemType;
import game.entity.mob.player.Player;
import game.graphics.Screen;
import game.graphics.Sprite;
import game.input.Mouse;

import java.util.ArrayList;
import java.util.List;

public class GUIInventory extends GUI
{
	private InventorySlotSection mainSection = new InventorySlotSection(ItemType.None, 5, 6, getXOffset() + 4, getYOffset() + 4, 1, 1);
	private InventorySlotSection skillSection = new InventorySlotSection(ItemType.Skill, 3, 1, getXOffset() + 95, getYOffset() + 89, 5, 0);
	private InventorySlotSection armourSection = new InventorySlotSection(ItemType.Armour, 2, 2, getXOffset() + 104, getYOffset() + 26, 6, 4);

	private List<InventorySlotSection> sections = new ArrayList<>();

	private ItemStack movingStack = new ItemStack();
	private boolean isMovingStack = false;

	public GUIInventory(Player player)
	{
		super(Sprite.INVENTORY, player);

		sections.add(mainSection);
		sections.add(skillSection);
		sections.add(armourSection);

		skillSection.getSlots().get(0).getStack().add(new ItemAbilityProjectileBullet(0, 0));
		updatePlayerSkills();
	}

	@Override
	protected void onClick()
	{
		sections.forEach(section ->
		{
			InventorySlot clickedSlot = section.getClickedSlot(Mouse.getX(), Mouse.getY());

			if (clickedSlot != null)
			{
				if (!movingStack.isEmpty())
				{
					if (!clickedSlot.getStack().merge(movingStack)) clickedSlot.getStack().swap(movingStack);
				}
				else clickedSlot.getStack().swap(movingStack);
			}
		});

		updatePlayerSkills();
	}

	public void render(Screen screen)
	{
		//Inventory
		screen.renderSprite(getXOffset(), getYOffset(), sprite, false);

		sections.forEach(section -> section.render(screen));
		movingStack.render(screen, Mouse.getX() / Game.SCALE - InventorySlot.SIZE / 2, Mouse.getY() / Game.SCALE - InventorySlot.SIZE / 2);
	}

	/**
	 * Attempt to add the item to the inventory. Return false if not successful and the inventory is full.
	 */
	public boolean addItem(Item item)
	{
		for (InventorySlot slot : mainSection.getSlots())
		{
			if (slot.getStack().add(item)) return true;
		}

		return false;
	}

	private void updatePlayerSkills()
	{
		for (int i = 0; i < 3; i++)
		{
			Item item = skillSection.getSlots().get(i).getStack().getItemInstance();
			if (item != null) player.setAbility(item.getItemAbility(player), i);
			else player.setAbility(null, i);
		}
	}

	public boolean armourEquipped()
	{
		//todo
		return false;
	}
}
