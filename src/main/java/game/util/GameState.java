package game.util;

public enum GameState
{
	TitleScreen, //Title screen
	Settings, ScreenInfo, //Miscellaneous
	IngameOffline, IngameOnline //Ingame
}