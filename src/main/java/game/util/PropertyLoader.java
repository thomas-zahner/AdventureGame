/*******************************************************************************
 * Copyright (C) 2018 Thomas Zahner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package game.util;

import game.Game;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class PropertyLoader
{
	private Properties properties = new Properties();
	private File file;

	private PropertyLoader(File file)
	{
		this.file = file;
	}

	public static PropertyLoader fromFile(String name)
	{
		File file = Game.getResource().getFile(name);
		return new PropertyLoader(file);
	}

	public String getValue(String property)
	{
		if (!file.exists()) return null;

		try
		{
			properties.load(file.toURI().toURL().openStream());
		}
		catch (IOException e)
		{
			return null;
		}

		return properties.getProperty(property);
	}
}
